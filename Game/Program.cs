﻿using System;

namespace Game
{
    class Program
    {
        static void Main(string[] args)
        {
            Cell[,] field = new Cell[Settings.width, Settings.height];          
            Console.WriteLine("Welcome to the Game!\nUse up, down, right and left arrows buttons to move\nGood luck!");
            Console.WriteLine("\nPress 1 to start or any to exit");
            if (Console.ReadLine() != "1")
                return;
            do
            {                
                CreateGame(field);
                int curX = 0;
                int curY = 0;
                int lives = Settings.livesCount;
                PrintField(field, curX, curY, lives);
                do
                {                    
                    Console.Write($"Go!");
                    (curX, curY) = Go(curX, curY);
                    field[curX, curY].state = CellStates.Open;
                    PrintField(field, curX, curY, lives);
                    CheckCell(field[curX, curY], ref lives);                                   
                }
                while (GameIsNotOver(field, curX, curY, ref lives));
                Console.WriteLine("\nPress 1 to start new game or any to exit");
                if ( Console.ReadLine() != "1")
                    break;
            }
            while (true);
        }

        static void CheckCell(Cell cell, ref int lives)
        {
            if (cell.type == CellType.Bomb)
            {
                Random rnd = new Random();
                int damage = rnd.Next(1, Settings.maxDamage);
                lives -= damage;
                Console.WriteLine($"Oops! Bomb! Damage {damage} lives");                
            }
        }

        static bool GameIsNotOver(Cell[,] cell, int curX, int curY, ref int lives)
        {
            if ((curX == (Settings.width - 1)) && (curY == (Settings.height - 1)))
            {
                Console.WriteLine("\nCongratulations! You win :)");
                return false;
            }
            if (lives <= 0)
            {
                Console.WriteLine("\nGame over! You lose :(");
                return false;
            }
            return true;
        }

        static (int, int) Go(int curX, int curY)
        {
            ConsoleKey key;
            int oldX = curX, oldY = curY;
            do
            {
                key = Console.ReadKey().Key;
                switch (key)
                {
                    case ConsoleKey.UpArrow:
                        --curX;
                        break;
                    case ConsoleKey.DownArrow:
                        ++curX;
                        break;
                    case ConsoleKey.LeftArrow:
                        --curY;
                        break;
                    case ConsoleKey.RightArrow:
                        ++curY;
                        break;
                    default: continue;
                }
                if (CheckPos(curX, curY))
                    return (curX, curY);
                else
                {
                    curX = oldX;
                    curY = oldY;
                    continue;
                }
            }             
            while (true);
        }

        static bool CheckPos(int curX, int curY)
        {
            if ((curX < 0) || (curX == Settings.width) || (curY < 0) || (curY == Settings.height))
                return false;
            else
                return true;
        }

        static void CreateGame(Cell[,] field)
        {
            for (int i = 0; i < Settings.width; ++i)
            {
                for (int j = 0; j < Settings.height; ++j)
                    field[i, j].NewCell();
            }
            field[0, 0].state = CellStates.Open;
            GenerateBombs(field);          
        }

        static void GenerateBombs(Cell[,] field)
        {
            int bombs = 0;
            Random rnd = new Random();
            while (bombs < Settings.bombCount)
            {
                int positionX = rnd.Next(0, Settings.width-1);
                int positionY = rnd.Next(0, Settings.height-1);
                if (((positionX == 0) && (positionY == 0)) || ((positionX == (Settings.width-1)) && (positionY == (Settings.height-1))))
                {
                    continue;
                }
                if (field[positionX, positionY].type != CellType.Bomb)
                {
                    field[positionX, positionY].type = CellType.Bomb;
                    ++bombs;
                }
            }
        }

        static void PrintField(Cell[,] field, int curX, int curY, int lives)
        {
            Console.Clear();
            string cellView;
            Console.WriteLine($"You have {lives} lives");
            for (int i = 0; i < Settings.width; ++i)
            {
                for (int j = 0; j < Settings.height; ++j)
                {
                    if ((i == curX) && (j == curY))
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        cellView = "*|";
                    }
                    else if (field[i, j].state == CellStates.Close)
                        cellView = " |";
                    else if (field[i, j].type == CellType.Bomb)
                    {
                        cellView = "x|";
                    }
                    else
                        cellView = ".|";
                    if ((field[i, j].type == CellType.Bomb) && (field[i, j].state == CellStates.Open))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                    }
                    Console.Write(cellView);
                    Console.ResetColor();
                }
                Console.WriteLine();
            }
        }
    }
}
