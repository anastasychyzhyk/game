﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Game
{
    enum CellStates
    {
        Open,
        Close
    }

    enum CellType
    {
        Empty,
        Bomb
    }
    struct Cell
    {
        public CellStates state;
        public CellType type;
        public void NewCell()
        {
            state = CellStates.Close;
            type = CellType.Empty;
        }
    }

    class Settings
    {
        public static int width { get; } = 10;
        public static int height { get; } = 10;
        public static int bombCount { get; } = 10;
        public static int livesCount { get; } = 10;
        public static int maxDamage { get; } = 10;
    }

}
